/*********************************************************************
 * Pico-X BSD
 * Copyright (C) 2020  Renzo Davoli <renzo@cs.unibo.it>, Daniele Lacamera <root@danielinux.net>
 * VirtualSquare team.
 *
 * SPDX-License-Identifier: GPL-2.0-only
 *
 * Pico-X BSD is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) version 3.
 *
 * Pico-X BSD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1335, USA
 *
 *
 *********************************************************************/
#include "frosted.h"
#include <sys/ioctl.h>
#include <pico_device.h>
#include <net/libnlq.h>
#include <socket_in.h>

static struct module mod_socket_nl;
extern struct pico_stack *PicoTCP;

struct picoxnl {
    struct fnode *node;
    struct task *task;
    int fd;
    struct nlq_msg *msgq;
};

static int sock_check_fd(int fd, struct fnode **fno)
{
    *fno = task_filedesc_get(fd);
    if (!fno)
        return -1;
    if (fd < 0)
        return -1;
    if ((*fno)->owner != &mod_socket_nl)
        return -1;
    return 0;
}
static struct picoxnl* fd_picoxnl(int fd)
{
    struct fnode *fno;
    struct picoxnl *s;
    if (sock_check_fd(fd, &fno) != 0)
        return NULL;

    s = (struct picoxnl *)fno->priv;
    return s;
}

static struct picoxnl *picoxnl_socket_new(void) {
	struct picoxnl *picoxnl = malloc(sizeof(struct picoxnl));
	if (picoxnl == NULL)
		return  NULL;
	picoxnl->msgq = NULL;
    picoxnl->fd = -1;
	return picoxnl;
}

static int socknl_poll(struct fnode *f, uint16_t events, uint16_t *revents)
{
    struct frosted_inet_socket *s;
    struct picoxnl *picoxnl = (struct picoxnl *)f->priv;
    if (!f || !picoxnl) {
        return -EINVAL;
    }
    if (nlq_length(picoxnl->msgq) != 0)
        *revents |= POLLIN;
    else
        *revents |= POLLOUT;
    return 0;
}

static int socknl_socket(int domain, int type_flags, int protocol)
{
    int fd = -1;
    struct picoxnl *s;
    int type = type_flags & 0xFFFF;
    uint32_t fnode_flags = ((uint32_t)type_flags) & 0xFFFF0000u;
    int ret = -1;
    if (mem_lock() < 0)
        return SYS_CALL_AGAIN;

    s = picoxnl_socket_new();
    if (!s) {
        ret = -ENOMEM;
        goto out;
    }
    s->node->owner = &mod_socket_nl;
    s->node->priv = s;
    s->fd = task_filedesc_add(s->node);
    if (s->fd >= 0)
        task_fd_setmask(s->fd, O_RDWR);
    ret = s->fd;
out:
    mem_unlock();
    return ret;
}

static int socknl_close(int fd) {
    struct picoxnl *s = fd_picoxnl(fd);
    if (!s) {
        return -EINVAL;
    }
	nlq_free(&s->msgq);
	free(s);
	return 0;
}

static int socknl_recvfrom(int fd, void *buf, unsigned int len, int flags,
		struct sockaddr *from, unsigned int *fromlen) {
	int retval = 0;
	int copylen = 0;
	struct nlq_msg *headmsg;
    struct picoxnl *picoxnl = fd_picoxnl(fd);
    if (!picoxnl) {
        return -EINVAL;
    }
    headmsg = nlq_head(picoxnl->msgq);
	if (headmsg == NULL)
		return errno = ENODATA, -1;
	if (len < headmsg->nlq_size) {
		if (flags & MSG_TRUNC)
			retval = headmsg->nlq_size;
		else
			retval = len;
		copylen = len;
	} else
		retval = copylen = headmsg->nlq_size;
	if (buf != NULL && copylen > 0)
		memcpy(buf, headmsg->nlq_packet, copylen);
	if (!(flags & MSG_PEEK)) {
		nlq_dequeue(&picoxnl->msgq);
		nlq_freemsg(headmsg);
	}
	if (from != NULL && fromlen != NULL&& *fromlen >= sizeof(struct sockaddr_nl)) {
		struct sockaddr_nl *rfrom = (struct sockaddr_nl *)from;
		struct sockaddr_nl sockname = {.nl_family = AF_NETLINK, .nl_pid = 0};
		*rfrom = sockname;
		*fromlen = sizeof(struct sockaddr_nl);
	}
	return retval;
}

static int socknl_sendto(int fd, void *buf, unsigned int len, int flags,
		struct sockaddr *to, unsigned int tolen) {

	struct nlmsghdr *msg = (struct nlmsghdr *)buf;
    struct picoxnl *picoxnl = fd_picoxnl(fd);
    if (!picoxnl) {
        return -EINVAL;
    }
	while (NLMSG_OK(msg, len)) {
		struct nlq_msg *msgq;
		msgq = picox_netlink_process(msg, PicoTCP);
		while (msgq != NULL) {
			struct nlq_msg *msg = nlq_dequeue(&msgq);
			nlq_enqueue(msg, &picoxnl->msgq);
		}
		msg = NLMSG_NEXT(msg, len);
	}

	return len;
}

static int socknl_bind(int fd, struct sockaddr *addr, unsigned int addrlen) {
	struct sockaddr_nl *raddr = (struct sockaddr_nl *) addr;
	static pid_t fakepid = 0;
    struct picoxnl *picoxnl = fd_picoxnl(fd);
    if (!picoxnl) {
        return -EINVAL;
    }
	if (addr == NULL)
		return errno = EFAULT, -1;
	if (addrlen < sizeof(*raddr) || raddr->nl_family != AF_NETLINK)
		return errno = EINVAL, -1;
	return 0;
}

static int socknl_getsockname (int fd, struct sockaddr *addr, unsigned int *addrlen) {
	struct sockaddr_nl *raddr = (struct sockaddr_nl *) addr;
	struct sockaddr_nl sockname = {.nl_family = AF_NETLINK, .nl_pid = 0};
    struct picoxnl *picoxnl = fd_picoxnl(fd);
    if (!picoxnl) {
        return -EINVAL;
    }
	if (addr == NULL || addrlen == NULL)
		return errno = EFAULT, -1;
	if (*addrlen < sizeof(sockname))
		return errno = EINVAL, -1;
	*raddr = sockname;
	*addrlen = sizeof(sockname);
	return 0;
}

static int socknl_ioctl(int fd, long cmd, void *argp) {
    struct picoxnl *picoxnl = fd_picoxnl(fd);
    if (!picoxnl) {
        return -EINVAL;
    }
	return picox_netlink_ioctl(PicoTCP, cmd, argp);
}

static int socknl_getpeername (int fd, struct sockaddr *addr, unsigned int *addrlen) {
	errno = EOPNOTSUPP;
	return -1;
}

static int socknl_getsockopt (int fd, int level, int optname, void *optval, unsigned int *optlen) {
	errno = EOPNOTSUPP;
	return -1;
}

static int socknl_setsockopt (int fd, int level, int optname, const void *optval, unsigned int optlen) {
    struct picoxnl *picoxnl = fd_picoxnl(fd);
    if (!picoxnl) {
        return -EINVAL;
    }
	switch (optname) {
		case SO_SNDBUF:
		case SO_RCVBUF:
			return 0;
	}
	return -EOPNOTSUPP;
}

static int socknl_connect(int fd, struct sockaddr *addr, unsigned int addrlen) {
	return -EOPNOTSUPP;
}



void socket_nl_init(void)
{
    mod_socket_nl.family = FAMILY_NETLINK;
    strcpy(mod_socket_nl.name,"picox-nl");

    mod_socket_nl.ops.poll = socknl_poll;
    mod_socket_nl.ops.close = socknl_close;

    mod_socket_nl.ops.socket     = socknl_socket;
   mod_socket_nl.ops.connect    = socknl_connect;
//   mod_socket_nl.ops.accept     = socknl_accept;
    mod_socket_nl.ops.bind       = socknl_bind;
//    mod_socket_nl.ops.listen     = socknl_listen;
    mod_socket_nl.ops.recvfrom   = socknl_recvfrom;
    mod_socket_nl.ops.sendto     = socknl_sendto;
//    mod_socket_nl.ops.shutdown   = socknl_shutdown;
    mod_socket_nl.ops.ioctl      = socknl_ioctl;
    mod_socket_nl.ops.getsockopt   = socknl_getsockopt;
    mod_socket_nl.ops.setsockopt   = socknl_setsockopt;
    mod_socket_nl.ops.getsockname   = socknl_getsockname;
    mod_socket_nl.ops.getpeername   = socknl_getpeername;

    register_module(&mod_socket_nl);
    register_addr_family(&mod_socket_nl, FAMILY_NETLINK);
}
